# Users

### Build and run

MacOs or Linux
```
./gradlew clean bootRun
```
or Windows
```
gradlew.bat clean bootRun
```

### Run from dockerfile

```
docker build -t users . && docker run --rm users -e POSTGRES_DB_HOST='localhost' -e POSTGRES_DB_PORT='5432' -e POSTGRES_DB_USERNAME='db_admin' -e POSTGRES_DB_PASSWORD='db_password' -p 80:8080
```