FROM gradle:jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle bootJar

FROM openjdk:17
WORKDIR /app
COPY --from=build /home/gradle/src/build/libs/users-0.0.1-SNAPSHOT.jar  /app/spring-boot-application.jar
ENTRYPOINT ["java", "-Xmx50m", "-jar", "/app/spring-boot-application.jar"]
EXPOSE 8080