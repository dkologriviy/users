#!/bin/sh
set -e
echo "waiting to start application"
sleep 1m
status=$(curl --silent --output /dev/stderr --write-out "%{http_code}" "$1""$SMOKE_TEST_URI")
if [ "$status" -ne 200 ]
then
    echo "Got status code $status instead of expected 200"
    exit 1
else
  echo "smoke is ok"
fi
