docker build -t users:1.0.0 .
kubectl create namespace users-dev
kubectl create namespace users-test
kubectl create namespace users-prod
helm install users-dev . -f values-dev.yaml
kubectl port-forward service/users-backend-service 8080:8080 --namespace=users-dev