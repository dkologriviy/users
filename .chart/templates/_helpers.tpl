{{- define "users.deployment" }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .appName }}-deployment
  namespace: {{ .namespace }}
  labels:
    app: {{ .appName }}
spec:
  replicas: {{ .replicasCount }}
  selector:
    matchLabels:
      app: {{ .appName }}
  template:
    metadata:
      labels:
        app: {{ .appName }}
    spec:
{{- if .isUsingDb }}
      initContainers:
      - name: check-db-ready
        image: postgres:13.3
        command: ['/bin/bash', '-c',
          'until pg_isready --host=$POSTGRES_DB_HOST --port=5432;
          do echo waiting for database; sleep 2; done;']
{{- if .container.env  }}
        env:
{{ toYaml .container.env | indent 8 }}
{{- end}}
{{- end }}
      containers:
      - name: {{ .appName }}
        image: {{ .container.image }}:{{ .container.version }}
{{- if eq .appName "users-db"}}
        lifecycle:
          postStart:
            exec:
              command: ["/bin/bash","-c","sleep 20 && PGPASSWORD=$POSTGRES_PASSWORD psql $POSTGRES_DB -U $POSTGRES_USER -c \'CREATE SCHEMA IF NOT EXISTS users_schema;\'"]
{{- end}}
        ports:
        - containerPort: {{ .container.port }}
        resources:
          requests:
            memory: 256M
            cpu: 100m
          limits:
            memory: 512M
            cpu: 200m
{{- if .container.env  }}
        env:
{{ toYaml .container.env | indent 8 }}
{{- end }}
{{- end }}
---
{{- define "users.service" }}
{{- if .service }}
apiVersion: v1
kind: Service
metadata:
  name: {{ .appName }}-service
  namespace: {{ .namespace }}
spec:
  selector:
    app: {{ .appName }}
  {{ if .service.type }}type: {{ .service.type }}{{ end }}
  ports:
  - protocol: TCP
    port: {{ .service.port }}
    targetPort: {{ .service.targetPort }}
    {{ if .service.nodePort }}nodePort: {{ .service.nodePort }}{{ end }}
{{- end}}
{{- end}}