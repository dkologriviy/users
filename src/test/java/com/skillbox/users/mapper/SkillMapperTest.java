package com.skillbox.users.mapper;

import com.skillbox.users.domain.Skill;
import com.skillbox.users.dto.SkillDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {SkillMapperImpl.class})
class SkillMapperTest {

    @Autowired
    SkillMapper skillMapper;

    private static final UUID ID = UUID.randomUUID();
    private static final String NAME = "Навык";
    Skill skill = new Skill(ID, NAME);

    @Test
    void toDtoTest() {
        SkillDto skillDto = skillMapper.toDto(skill);
        assertEquals(ID, skillDto.getId());
        assertEquals(NAME, skillDto.getName());
    }

    @Test
    void listToDtoList() {
        List<Skill> skillList = new ArrayList<>();
        skillList.add(skill);

        List<SkillDto> skillDtoList = skillMapper.listToDtoList(skillList);

        assertEquals(1, skillDtoList.size());
        assertTrue(skillDtoList.stream().anyMatch(skillDto -> skillDto.getId() == ID));
        assertTrue(skillDtoList.stream().anyMatch(skillDto -> skillDto.getName().equals(NAME)));
    }

}