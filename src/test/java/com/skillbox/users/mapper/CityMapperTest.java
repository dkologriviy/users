package com.skillbox.users.mapper;

import com.skillbox.users.domain.City;
import com.skillbox.users.dto.CityDto;
import com.skillbox.users.service.CityService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CityMapperImpl.class})
class CityMapperTest {

    @Autowired
    private CityMapper cityMapper;

    @MockBean
    private CityService cityService;

    private static final UUID ID = UUID.randomUUID();
    private static final String CITY_NAME = "Moscow";
    CityDto cityDto = new CityDto(ID, CITY_NAME);
    City city = new City(ID, CITY_NAME, false);

    @BeforeAll
    static void init() {

    }

    @Test
    void cityDtoToDtoTest() {
        CityDto mappedCityDto = cityMapper.toDto(city);
        assertEquals(ID, mappedCityDto.getId());
        assertEquals(CITY_NAME, mappedCityDto.getName());
    }

    @Test
    void cityToDomainTest() {
        //When
        Mockito.when(cityService.getNameById(ID)).thenReturn(CITY_NAME);

        //Then
        City mappedCity = cityMapper.toDomain(cityDto);
        assertEquals(ID, mappedCity.getId());
        assertEquals(CITY_NAME, mappedCity.getName());
    }

    @Test
    void listToDtoListTest() {
        //Given
        List<City> cityList = new ArrayList<>();
        cityList.add(city);

        //When
        Mockito.when(cityService.getNameById(ID)).thenReturn(CITY_NAME);

        //Then
        List<CityDto> cityDtoList = cityMapper.listToDtoList(cityList);

        assertEquals(1, cityDtoList.size());
        assertTrue(cityDtoList.stream().anyMatch(city -> city.getId() == ID));
        assertTrue(cityDtoList.stream().anyMatch(city -> city.getName().equals(CITY_NAME)));
    }
}