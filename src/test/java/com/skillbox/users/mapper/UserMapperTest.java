package com.skillbox.users.mapper;

import com.skillbox.users.domain.City;
import com.skillbox.users.domain.Gender;
import com.skillbox.users.domain.Skill;
import com.skillbox.users.domain.User;
import com.skillbox.users.dto.NewUserDto;
import com.skillbox.users.dto.UserCompleteDto;
import com.skillbox.users.dto.UserDto;
import com.skillbox.users.service.CityService;
import com.skillbox.users.service.UserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {UserMapperImpl.class, CityMapperImpl.class, SkillMapperImpl.class})
class UserMapperTest {

    @MockBean
    private CityService cityService;

    @MockBean
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private CityMapper cityMapper;

    @Autowired
    private SkillMapper skillMapper;

    private static final UUID ID = UUID.randomUUID();
    private static final String FIRST_NAME = "Иван";
    private static final String MIDDLE_NAME = "Иванов";
    private static final String LAST_NAME = "Иванович";
    private static final Gender GENDER = Gender.FEMALE;
    private static final LocalDate BIRTHDAY = LocalDate.of(1988, 1, 1);
    private static final City CITY = new City(UUID.randomUUID(), "Moscow", false);
    private static final City CITY2 = new City(UUID.randomUUID(), "Samara", false);
    private static final String AVATAR_URL = "https://avatar.ru/user.img";
    private static final String ADDITIONAL_INFO = "user information";
    private static final String NICKNAME = "iivanov";
    private static final String EMAIL = "iivanov@gmail.com";
    private static final String PHONE = "9166453231";
    private static final Skill SKILL = new Skill(UUID.randomUUID(), "Skill_1");


    static User user = new User(
            ID,
            FIRST_NAME,
            MIDDLE_NAME,
            LAST_NAME,
            GENDER,
            BIRTHDAY,
            CITY,
            AVATAR_URL,
            ADDITIONAL_INFO,
            NICKNAME,
            EMAIL,
            PHONE,
            List.of(SKILL),
            new ArrayList<>(),
            new ArrayList<>(),
            false);

    static User subscribedToUser = new User(
            UUID.randomUUID(),
            "Фёдор",
            "Федоровский",
            "Фёдорович",
            Gender.FEMALE,
            LocalDate.of(1985, 11, 1),
            CITY2,
            null,
            null,
            "fff",
            "fff@gmail.com",
            "9116453211",
            new ArrayList<>(),
            new ArrayList<>(),
            new ArrayList<>(),
            false);

    static User subscriber = new User(
            UUID.randomUUID(),
            "Петр",
            "Петрович",
            "Петров",
            Gender.FEMALE,
            LocalDate.of(1994, 12, 12),
            CITY2,
            null,
            null,
            "iivanov",
            "iivanov@gmail.com",
            "9166453231",
            new ArrayList<>(),
            new ArrayList<>(),
            new ArrayList<>(),
            false);

    @BeforeAll
    public static void init() {
        user.getSubscribedTo().add(subscribedToUser);
        user.getSubscribers().add(subscriber);
    }

    @Test
    void toDto() {
        UserDto userDto = userMapper.toDto(user);

        assertEquals(ID, userDto.getId());
        assertEquals(FIRST_NAME, userDto.getFirstName());
        assertEquals(MIDDLE_NAME, userDto.getMiddleName());
        assertEquals(LAST_NAME, userDto.getLastName());
        assertEquals(GENDER, userDto.getGender());
        assertEquals(BIRTHDAY, userDto.getBirthday());
        assertEquals(cityMapper.toDto(CITY), userDto.getCity());
        assertEquals(AVATAR_URL, userDto.getAvatarUrl());
        assertEquals(ADDITIONAL_INFO, userDto.getAdditionalInfo());
        assertEquals(NICKNAME, userDto.getNickname());
        assertEquals(EMAIL, userDto.getEmail());
        assertEquals(PHONE, userDto.getPhone());
    }

    @Test
    void toCompleteDto() {
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(user);

        assertEquals(ID, userCompleteDto.getId());
        assertEquals(FIRST_NAME, userCompleteDto.getFirstName());
        assertEquals(MIDDLE_NAME, userCompleteDto.getMiddleName());
        assertEquals(LAST_NAME, userCompleteDto.getLastName());
        assertEquals(GENDER, userCompleteDto.getGender());
        assertEquals(BIRTHDAY, userCompleteDto.getBirthday());
        assertEquals(cityMapper.toDto(CITY), userCompleteDto.getCity());
        assertEquals(AVATAR_URL, userCompleteDto.getAvatarUrl());
        assertEquals(ADDITIONAL_INFO, userCompleteDto.getAdditionalInfo());
        assertEquals(NICKNAME, userCompleteDto.getNickname());
        assertEquals(EMAIL, userCompleteDto.getEmail());
        assertEquals(PHONE, userCompleteDto.getPhone());

        assertTrue(userCompleteDto.getSkills().contains(skillMapper.toDto(SKILL)));
        assertEquals(1, userCompleteDto.getSkills().size());

        assertTrue(userCompleteDto.getSubscribedTo().contains(userMapper.toDto(subscribedToUser)));
        assertEquals(1, userCompleteDto.getSubscribedTo().size());

        assertTrue(userCompleteDto.getSubscribers().contains(userMapper.toDto(subscriber)));
        assertEquals(1, userCompleteDto.getSubscribers().size());
    }

    @Test
    void toDomain() {
        UserDto userDto = userMapper.toDto(user);
        Mockito.when(userService.getById(ID)).thenReturn(user);
        Mockito.when(cityService.getById(CITY.getId())).thenReturn(CITY);
        Mockito.when(cityService.getNameById(CITY.getId())).thenReturn(CITY.getName());

        User result = userMapper.toDomain(userDto);
        assertEquals(user.getId(), result.getId());
        assertEquals(user.getFirstName(), result.getFirstName());
        assertEquals(user.getMiddleName(), result.getMiddleName());
        assertEquals(user.getLastName(), result.getLastName());
        assertEquals(user.getGender(), result.getGender());
        assertEquals(user.getBirthday(), result.getBirthday());
        assertEquals(user.getAvatarUrl(), result.getAvatarUrl());
        assertEquals(user.getAdditionalInfo(), result.getAdditionalInfo());
        assertEquals(user.getNickname(), result.getNickname());
        assertEquals(user.getEmail(), result.getEmail());
        assertEquals(user.getPhone(), result.getPhone());
        assertEquals(user.getCity(), result.getCity());
        assertEquals(user.getSkills(), result.getSkills());
        assertEquals(user.getSubscribers(), result.getSubscribers());
        assertEquals(user.getSubscribedTo(), result.getSubscribedTo());
    }

    @Test
    void newUserDtoToDomain() {
        NewUserDto newUserDto = new NewUserDto(
                FIRST_NAME,
                MIDDLE_NAME,
                LAST_NAME,
                GENDER,
                BIRTHDAY,
                CITY.getId(),
                AVATAR_URL,
                ADDITIONAL_INFO,
                NICKNAME,
                EMAIL,
                PHONE
        );
        Mockito.when(cityService.getById(CITY.getId())).thenReturn(CITY);
        Mockito.when(cityService.getNameById(CITY.getId())).thenReturn(CITY.getName());

        User result = userMapper.toDomain(newUserDto);
        assertEquals(newUserDto.getFirstName(), result.getFirstName());
        assertEquals(newUserDto.getMiddleName(), result.getMiddleName());
        assertEquals(newUserDto.getLastName(), result.getLastName());
        assertEquals(newUserDto.getGender(), result.getGender());
        assertEquals(newUserDto.getBirthday(), result.getBirthday());
        assertEquals(newUserDto.getAvatarUrl(), result.getAvatarUrl());
        assertEquals(newUserDto.getAdditionalInfo(), result.getAdditionalInfo());
        assertEquals(newUserDto.getNickname(), result.getNickname());
        assertEquals(newUserDto.getEmail(), result.getEmail());
        assertEquals(newUserDto.getPhone(), result.getPhone());
        assertEquals(newUserDto.getCityId(), result.getCity().getId());
    }

    @Test
    void listToDtoList() {
        List<User> users = List.of(user, subscriber, subscribedToUser);
        List<UserDto> userDtoList = userMapper.listToDtoList(users);

        assertTrue(userDtoList.contains(userMapper.toDto(user)));
        assertTrue(userDtoList.contains(userMapper.toDto(subscriber)));
        assertTrue(userDtoList.contains(userMapper.toDto(subscribedToUser)));
    }
}