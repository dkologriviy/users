package com.skillbox.users.service;

import com.skillbox.users.domain.City;
import com.skillbox.users.domain.Gender;
import com.skillbox.users.domain.Skill;
import com.skillbox.users.domain.User;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.SkillRepository;
import com.skillbox.users.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class SkillServiceTest {

    @Mock
    private SkillRepository skillRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private SkillService skillService;

    //GIVEN
    private final UUID existSkillId = UUID.randomUUID();
    private final String existSkillName = "умение 1";
    private final Skill existSkill = new Skill(existSkillId, existSkillName);

    private final UUID existSkillTwoId = UUID.randomUUID();
    private final String existSkillTwoName = "умение 1";
    private final Skill existSkillTwo = new Skill(existSkillTwoId, existSkillTwoName);

    private final UUID newSkillId = UUID.randomUUID();
    private final String newSkillName = "умение 2";
    private final Skill newSkill = new Skill(newSkillId, newSkillName);


    @Test
    void getAll() {
        //when
        Mockito.when(skillRepository.findAll()).thenReturn(List.of(existSkill));

        //then
        List<Skill> skills = skillService.getAll();
        Assertions.assertTrue(skills.contains(existSkill));
        Assertions.assertFalse(skills.contains(newSkill));
    }

    @Test
    void getById() {
        //when
        Mockito.when(skillRepository.findById(existSkillId)).thenReturn(Optional.ofNullable(existSkill));
        Mockito.when(skillRepository.findById(newSkillId)).thenReturn(Optional.ofNullable(null));

        //then
        Executable getNotExistSkill = () -> skillService.getById(newSkillId);

        Assertions.assertEquals(existSkill, skillService.getById(existSkillId));
        Assertions.assertThrows(ObjectNotFoundException.class, getNotExistSkill);
    }

    @Test
    void delete() {
        //Given
        City city = new City(UUID.randomUUID(), "Москва", false);
        User existUser = new User(
                UUID.randomUUID(),
                "Иван",
                "Иванов",
                "Иванович",
                Gender.FEMALE,
                LocalDate.of(1988, 1, 1),
                city,
                null,
                null,
                "iivanov",
                "iivanov@gmail.com",
                "9166453231",
                new ArrayList<>(List.of(existSkill, existSkillTwo)),
                new ArrayList<>(),
                new ArrayList<>(),
                false);

        User existUserTwo = new User(
                UUID.randomUUID(),
                "Петр",
                "Петрович",
                "Петров",
                Gender.FEMALE,
                LocalDate.of(1994, 12, 12),
                city,
                null,
                null,
                "iivanov",
                "iivanov@gmail.com",
                "9166453231",
                new ArrayList<>(List.of(existSkill, existSkillTwo)),
                new ArrayList<>(),
                new ArrayList<>(),
                false);

        //WHEN
        Mockito.when(userRepository.getUsersBySkillId(existSkillId))
                .thenReturn(List.of(existUser, existUserTwo));
        Mockito.when(skillRepository.findById(existSkillId))
                .thenReturn(Optional.of(existSkill));

        //THEN
        skillService.delete(existSkillId);
        Assertions.assertFalse(existUser.getSkills().contains(existSkill));
        Assertions.assertTrue(existUser.getSkills().contains(existSkillTwo));

        Assertions.assertFalse(existUserTwo.getSkills().contains(existSkill));
        Assertions.assertTrue(existUserTwo.getSkills().contains(existSkillTwo));
    }

    @Test
    void add() {
        //when
        Mockito.when(skillRepository.save(any()))
                .thenAnswer(invocation -> {
                    Skill skill = invocation.getArgument(0);
                    if (skill.getName().equals(newSkillName)) {
                        return newSkill;
                    }
                    return null;
                });
        Mockito.when(skillRepository.findSkillByName(any()))
                .thenAnswer(invocation -> {
                    String skillToAddName = invocation.getArgument(0);
                    if (skillToAddName.equals(existSkillName)) {
                        return Optional.of(existSkill);
                    }
                    return Optional.ofNullable(null);
                });

        //then
        Executable addExistingSkill = () -> skillService.add(existSkillName);

        Executable addExistingSkillToUpper = () -> skillService.add(existSkillName.toUpperCase());

        Assertions.assertEquals(newSkill, skillService.add(newSkillName));
        Assertions.assertThrows(ObjectAlreadyExistException.class, addExistingSkill);
        Assertions.assertThrows(ObjectAlreadyExistException.class, addExistingSkillToUpper);
    }
}