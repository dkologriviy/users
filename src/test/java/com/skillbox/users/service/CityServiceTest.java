package com.skillbox.users.service;

import com.skillbox.users.domain.City;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.CityRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class CityServiceTest {

    @Mock
    private CityRepository cityRepository;

    @Mock
    private UserService userService;

    @InjectMocks
    private CityService cityService;

    private final UUID existCityId = UUID.randomUUID();
    private final String existCityName = "Москва";
    City existCity = new City(existCityId, existCityName, false);

    private final UUID newCityId = UUID.randomUUID();
    private final String newCityName = "Питер";
    City newCity = new City(newCityId, newCityName, false);

    @Test
    void getAll() {
        //when
        Mockito.when(cityRepository.findAll()).thenReturn(List.of(existCity));

        //then
        List<City> cityList = cityService.getAll();

        Assertions.assertTrue(cityList.contains(existCity));
        Assertions.assertFalse(cityList.contains(newCity));
    }

    @Test
    void add() {
        //when
        Mockito.when(cityRepository.findCityByName(existCityName)).thenReturn(Optional.ofNullable(existCity));
        Mockito.when(cityRepository.findCityByName(newCityName)).thenReturn(Optional.ofNullable(null));

        Mockito.when(cityRepository.save(any()))
                .thenAnswer(invocation -> {
                    City city = invocation.getArgument(0);
                    if (city.getName().equals(newCity.getName())) {
                        return newCity;
                    }
                    return null;
                });

        //then
        Executable addCity = () -> cityService.add(existCityName);
        Assertions.assertThrows(ObjectAlreadyExistException.class, addCity);
        Assertions.assertEquals(newCity, cityService.add(newCityName));
    }

    @Test
    void delete() {
        //when
        Mockito.when(cityRepository.findById(existCityId)).thenReturn(Optional.ofNullable(existCity));
        Mockito.when(cityRepository.findById(newCityId)).thenReturn(Optional.ofNullable(null));

        //then
        Executable deleteExistCity = () -> cityService.delete(existCityId);
        Executable deleteNewCity = () -> cityService.delete(newCityId);

        //then
        Assertions.assertDoesNotThrow(deleteExistCity);
        Assertions.assertThrows(ObjectNotFoundException.class, deleteNewCity);
    }

    @Test
    void getNameById() {
        //when
        Mockito.when(cityRepository.findById(existCityId)).thenReturn(Optional.ofNullable(existCity));
        Mockito.when(cityRepository.findById(newCityId)).thenReturn(Optional.ofNullable(null));

        //then
        Executable getNewCityName = () -> cityService.getNameById(newCityId);
        Assertions.assertEquals(existCityName, cityService.getNameById(existCityId));
        Assertions.assertThrows(ObjectNotFoundException.class, getNewCityName);
    }

    @Test
    void getById() {
        //when
        Mockito.when(cityRepository.findById(existCityId)).thenReturn(Optional.ofNullable(existCity));
        Mockito.when(cityRepository.findById(newCityId)).thenReturn(Optional.ofNullable(null));

        //then
        Executable getNewCity = () -> cityService.getById(newCityId);
        Assertions.assertEquals(existCity, cityService.getById(existCityId));
        Assertions.assertThrows(ObjectNotFoundException.class, getNewCity);
    }
}