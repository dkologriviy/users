package com.skillbox.users.service;

import com.skillbox.users.domain.City;
import com.skillbox.users.domain.Gender;
import com.skillbox.users.domain.Skill;
import com.skillbox.users.domain.User;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SkillService skillService;

    @InjectMocks
    private UserService userService;

    UUID existCityId = UUID.randomUUID();
    City existCity = new City(existCityId, "Москва", false);

    UUID existUserId = UUID.randomUUID();
    private User existUser;

    UUID existUserTwoId = UUID.randomUUID();
    private User existUserTwo;

    UUID newUserId = UUID.randomUUID();
    private User newUser;

    UUID skillId = UUID.randomUUID();
    Skill skill = new Skill(skillId, "Навык 1");


    @BeforeEach
    void init() {
        existUser = new User(
                existUserId,
                "Иван",
                "Иванов",
                "Иванович",
                Gender.FEMALE,
                LocalDate.of(1988, 1, 1),
                existCity,
                null,
                null,
                "iivanov",
                "iivanov@gmail.com",
                "9166453231",
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                false);

        existUserTwo = new User(
                existUserTwoId,
                "Фёдор",
                "Федоровский",
                "Фёдорович",
                Gender.FEMALE,
                LocalDate.of(1985, 11, 1),
                existCity,
                null,
                null,
                "fff",
                "fff@gmail.com",
                "9116453211",
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                false);

        newUser = new User(
                newUserId,
                "Петр",
                "Петрович",
                "Петров",
                Gender.FEMALE,
                LocalDate.of(1994, 12, 12),
                existCity,
                null,
                null,
                "iivanov",
                "iivanov@gmail.com",
                "9166453231",
                new ArrayList<>(),
                new ArrayList<>(),
                new ArrayList<>(),
                false);
    }

    @Test
    void getUsersByCityId() {
        //when
        Mockito.when(userRepository.getUsersByCityId(not(eq(existCityId))))
                .thenReturn(List.of());

        Mockito.when(userRepository.getUsersByCityId(existCityId))
                .thenReturn(List.of(existUser));

        //then
        assertTrue(userService.getUsersByCityId(existCityId).contains(existUser));
        assertFalse(userService.getUsersByCityId(UUID.randomUUID()).contains(existUser));
    }

    @Test
    void getById() {
        //when
        Mockito.when(userRepository.findById(existUserId))
                .thenReturn(Optional.ofNullable(existUser));
        Mockito.when(userRepository.findById(not(eq(existUserId))))
                .thenThrow(ObjectNotFoundException.class);

        //then
        assertEquals(existUser, userService.getById(existUserId));

        Executable getNotExistUser = () -> userService.getById(UUID.randomUUID());
        assertThrows(ObjectNotFoundException.class, getNotExistUser);

    }

    @Test
    void getAll() {
        //when
        Mockito.when(userRepository.findAll()).thenReturn(List.of(existUser));

        //then
        List<User> users = userService.getAll();

        assertTrue(users.contains(existUser));
        assertFalse(users.contains(newUser));
    }

    @Test
    void add() {
        Mockito.when(userRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));
        assertEquals(newUser, userService.add(newUser));
    }

    @Test
    void update() {
        Mockito.when(userRepository.save(any()))
                .thenAnswer(invocation -> invocation.getArgument(0));
        assertEquals(existUser, userService.update(existUser));
    }

    @Test
    void subscribeTo() {
        //when
        Mockito.when(userRepository.findById(existUserId))
                .thenReturn(Optional.of(existUser));
        Mockito.when(userRepository.findById(existUserTwoId))
                .thenReturn(Optional.of(existUserTwo));

        //then
        Executable subscribe = () -> userService.subscribeTo(existUserId, existUserTwoId);

        //subscribe for the first time
        assertDoesNotThrow(subscribe);
        assertTrue(existUser.getSubscribedTo().contains(existUserTwo));

        //subscribe for the second time
        assertThrows(ObjectAlreadyExistException.class, subscribe);
    }

    @Test
    void unsubscribe() {
        //given
        existUser.getSubscribedTo().add(existUserTwo);

        //when
        Mockito.when(userRepository.findById(existUserId))
                .thenReturn(Optional.of(existUser));
        Mockito.when(userRepository.findById(existUserTwoId))
                .thenReturn(Optional.of(existUserTwo));

        //then
        Executable unsubscribe = () -> userService.unsubscribe(existUserId, existUserTwoId);

        //unsubscribe for the first time
        assertDoesNotThrow(unsubscribe);
        assertFalse(existUser.getSubscribedTo().contains(existUserTwo));

        //unsubscribe for the second time
        assertThrows(ObjectNotFoundException.class, unsubscribe);
    }

    @Test
    void addSkill() {
        //when
        Mockito.when(userRepository.findById(existUserId))
                .thenReturn(Optional.of(existUser));
        Mockito.when(skillService.getById(skillId))
                .thenReturn(skill);

        //then
        Executable addSkill = () -> userService.addSkill(existUserId, skillId);

        //add skill for the first time
        assertDoesNotThrow(addSkill);
        assertTrue(existUser.getSkills().contains(skill));

        //add skill for the second time
        assertThrows(ObjectAlreadyExistException.class, addSkill);
    }

    @Test
    void deleteSkill() {
        //given
        existUser.getSkills().add(skill);

        //when
        Mockito.when(userRepository.findById(existUserId))
                .thenReturn(Optional.of(existUser));
        Mockito.when(skillService.getById(skillId))
                .thenReturn(skill);

        //then
        Executable deleteSkill = () -> userService.deleteSkill(existUserId, skillId);

        //delete skill for the first time
        assertDoesNotThrow(deleteSkill);
        assertFalse(existUser.getSkills().contains(skill));

        //delete skill for the second time
        assertThrows(ObjectNotFoundException.class, deleteSkill);
    }

}