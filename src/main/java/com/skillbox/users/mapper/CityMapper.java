package com.skillbox.users.mapper;

import com.skillbox.users.domain.City;
import com.skillbox.users.service.CityService;
import org.mapstruct.Mapper;
import com.skillbox.users.dto.CityDto;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class CityMapper {

    @Autowired
    protected CityService cityService;

    public abstract CityDto toDto(City city);

    public abstract List<CityDto> listToDtoList(List<City> cities);

    @Mapping(target = "deleted", ignore = true)
    @Mapping(target = "name", expression = "java(cityService.getNameById(cityDto.getId()))")
    public abstract City toDomain(CityDto cityDto);

}
