package com.skillbox.users.mapper;

import com.skillbox.users.domain.Skill;
import com.skillbox.users.dto.SkillDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SkillMapper {

    SkillDto toDto(Skill skill);

    List<SkillDto> listToDtoList(List<Skill> skills);
}
