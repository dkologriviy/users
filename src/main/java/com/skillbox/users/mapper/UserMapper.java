package com.skillbox.users.mapper;

import com.skillbox.users.domain.User;
import com.skillbox.users.dto.NewUserDto;
import com.skillbox.users.dto.UserCompleteDto;
import com.skillbox.users.dto.UserDto;
import com.skillbox.users.service.CityService;
import com.skillbox.users.service.UserService;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(
        componentModel = "spring",
        uses = {CityMapper.class, SkillMapper.class})
public abstract class UserMapper {

    @Autowired
    private UserService userService;

    @Autowired
    protected CityService cityService;

    public abstract UserDto toDto(User user);

    public abstract UserCompleteDto toCompleteDto(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "subscribedTo", ignore = true)
    @Mapping(target = "subscribers", ignore = true)
    @Mapping(target = "deleted", ignore = true)
    @Mapping(target = "skills", ignore = true)
    @Mapping(target = "city", expression = "java(cityService.getById(newUserDto.getCityId()))")
    public abstract User toDomain(NewUserDto newUserDto);

    @Mapping(target = "subscribedTo", ignore = true)
    @Mapping(target = "subscribers", ignore = true)
    @Mapping(target = "deleted", ignore = true)
    @Mapping(target = "skills", ignore = true)
    public abstract User toDomain(UserDto userDto);

    @BeforeMapping
    protected void enrichDomain(UserDto userDto, @MappingTarget User user) {
        User tempUser = userService.getById(userDto.getId());
        user.setSkills(tempUser.getSkills());
        user.setSubscribers(tempUser.getSubscribers());
        user.setSubscribedTo(tempUser.getSubscribedTo());
    }

    public abstract List<UserDto> listToDtoList(List<User> users);

}
