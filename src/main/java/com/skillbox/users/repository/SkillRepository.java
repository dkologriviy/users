package com.skillbox.users.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.skillbox.users.domain.Skill;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface SkillRepository extends CrudRepository<Skill, UUID> {
    List<Skill> findAll();

    Optional<Skill> findSkillByName(String name);
}
