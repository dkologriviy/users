package com.skillbox.users.repository;

import com.skillbox.users.domain.City;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CityRepository extends CrudRepository<City, UUID> {
    List<City> findAll();

    Optional<City> findCityByName(String name);
}
