package com.skillbox.users.repository;

import com.skillbox.users.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User, UUID> {

    @Query(value = "SELECT u FROM User u WHERE u.city.id = ?1")
    List<User> getUsersByCityId(UUID cityId);

    @Query(value = "SELECT u FROM User u JOIN u.skills s WHERE s.id =?1")
    List<User> getUsersBySkillId(UUID skillId);

    List<User> findAll();

    @Query(value = "SELECT u FROM User u WHERE u.email=?1")
    Optional<User> findUserByEmail(String email);
}
