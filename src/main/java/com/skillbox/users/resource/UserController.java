package com.skillbox.users.resource;

import com.skillbox.users.domain.User;
import com.skillbox.users.dto.NewUserDto;
import com.skillbox.users.dto.SubscribeToDto;
import com.skillbox.users.dto.UnsubscribeDto;
import com.skillbox.users.dto.UserCompleteDto;
import com.skillbox.users.dto.UserDto;
import com.skillbox.users.dto.UserSkillDto;
import com.skillbox.users.mapper.UserMapper;
import com.skillbox.users.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Valid
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/user")
@Slf4j
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping("/getAll")
    public List<UserDto> getAll() {
        log.debug("getAll() - start");
        List<UserDto> result = userMapper.listToDtoList(userService.getAll());
        log.debug("getAll() - end: result={}", result);
        return result;
    }

    @GetMapping("/{userId}")
    public UserCompleteDto getById(@Valid @PathVariable UUID userId) {
        log.debug("getById() - start: userId={}", userId);
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(userService.getById(userId));
        log.debug("getById() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @PostMapping("/add")
    public UserCompleteDto add(@Valid @RequestBody NewUserDto newUserDto) {
        log.debug("add() - start: newUserDto={}", newUserDto);
        User addedUser = userService.add(userMapper.toDomain(newUserDto));
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(addedUser);
        log.debug("add() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @PutMapping("/update")
    public UserCompleteDto update(@Valid @RequestBody UserDto userDto) {
        log.debug("update() - start: userDto={}", userDto);
        User updatedUser = userService.update(userMapper.toDomain(userDto));
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(updatedUser);
        log.debug("update() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @PostMapping("/subscribe")
    public UserCompleteDto subscribeTo(@Valid @RequestBody SubscribeToDto subscribeToDto) {
        log.debug("subscribeTo() - start: subscribeToDto={}", subscribeToDto);
        User updatedUser = userService.subscribeTo(subscribeToDto.getUserId(), subscribeToDto.getSubscribeToUserId());
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(updatedUser);
        log.debug("subscribeTo() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @PostMapping("/unsubscribe")
    public UserCompleteDto unsubscribe(@Valid @RequestBody UnsubscribeDto unsubscribeDto) {
        log.debug("unsubscribe() - start: unsubscribeDto={}", unsubscribeDto);
        User updatedUser = userService.unsubscribe(unsubscribeDto.getUserId(), unsubscribeDto.getUnsubscribeFromUserId());
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(updatedUser);
        log.debug("unsubscribe() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @PostMapping("/skill")
    public UserCompleteDto addSkill(@Valid @RequestBody UserSkillDto userSkillDto) {
        log.debug("addSkill() - start: userSkillDto={}", userSkillDto);
        User updatedUser = userService.addSkill(userSkillDto.getUserId(), userSkillDto.getSkillId());
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(updatedUser);
        log.debug("addSkill() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }

    @DeleteMapping("/skill")
    public UserCompleteDto deleteSkill(@Valid @RequestBody UserSkillDto userSkillDto) {
        log.debug("deleteSkill() - start: userSkillDto={}", userSkillDto);
        User updatedUser = userService.deleteSkill(userSkillDto.getUserId(), userSkillDto.getSkillId());
        UserCompleteDto userCompleteDto = userMapper.toCompleteDto(updatedUser);
        log.debug("deleteSkill() - end: userCompleteDto={}", userCompleteDto);
        return userCompleteDto;
    }
}
