package com.skillbox.users.resource;

import com.skillbox.users.domain.Skill;
import com.skillbox.users.dto.NewSkillDto;
import com.skillbox.users.dto.SkillDto;
import com.skillbox.users.mapper.SkillMapper;
import com.skillbox.users.service.SkillService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/skill")
@Slf4j
public class SkillController {

    private final SkillService skillService;
    private final SkillMapper mapper = Mappers.getMapper(SkillMapper.class);

    @GetMapping("/getAll")
    public List<SkillDto> getAll() {
        log.debug("getAll() - start");
        List<SkillDto> result = mapper.listToDtoList(skillService.getAll());
        log.debug("getAll() - end: response={}", result);
        return result;
    }

    @PostMapping("/add")
    public SkillDto add(@Valid @RequestBody NewSkillDto newSkillDto) {
        log.debug("add() - start: newSkillDto={}", newSkillDto);
        Skill skill = skillService.add(newSkillDto.getName());
        SkillDto skillDto = mapper.toDto(skill);
        log.debug("add() - end: skillDto={}", skillDto);
        return skillDto;
    }

    @DeleteMapping("/{skillIdToDelete}")
    public ResponseEntity<UUID> delete(@PathVariable UUID skillIdToDelete) {
        log.debug("delete() - start: skillIdToDelete={}", skillIdToDelete);
        skillService.delete(skillIdToDelete);
        log.debug("delete() - end");
        return new ResponseEntity<>(skillIdToDelete, HttpStatus.OK);
    }
}
