package com.skillbox.users.resource;

import com.skillbox.users.domain.City;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.skillbox.users.dto.CityDto;
import com.skillbox.users.dto.NewCityDto;
import com.skillbox.users.mapper.CityMapper;
import com.skillbox.users.service.CityService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/city")
@RequiredArgsConstructor
@Slf4j
public class CityController {

    private final CityService cityService;
    private final CityMapper mapper = Mappers.getMapper(CityMapper.class);

    @GetMapping("/getAll")
    public List<CityDto> getAll() {
        log.debug("getAll() - start");
        List<CityDto> result = mapper.listToDtoList(cityService.getAll());
        log.debug("getAll() - end: result={}", result);
        return result;
    }

    @PostMapping("/add")
    public CityDto add(@Valid @RequestBody NewCityDto newCityDto) {
        log.debug("add() - start : newCityDto={}", newCityDto);
        City city = cityService.add(newCityDto.getName());
        CityDto cityDto = mapper.toDto(city);
        log.debug("add() - end: cityDto={}", cityDto);
        return cityDto;
    }

    @DeleteMapping("/{cityIdToDelete}")
    public ResponseEntity<UUID> delete(@PathVariable UUID cityIdToDelete) {
        log.debug("delete() - starts: cityIdToDelete={}", cityIdToDelete);
        cityService.delete(cityIdToDelete);
        log.debug("delete() - end");
        return new ResponseEntity<>(cityIdToDelete, HttpStatus.OK);
    }
}
