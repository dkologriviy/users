package com.skillbox.users.handler;

import com.skillbox.users.dto.ErrorDto;
import com.skillbox.users.exception.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.skillbox.users.exception.ObjectAlreadyExistException;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(value = {ObjectAlreadyExistException.class})
    protected ResponseEntity<ErrorDto> handleUnprocessable(
            RuntimeException ex) {
        ErrorDto errorDto = new ErrorDto(LocalDateTime.now(), ex.getMessage(), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDto, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(value = {ObjectNotFoundException.class})
    protected ResponseEntity<ErrorDto> handleNotFound(
            RuntimeException ex) {
        ErrorDto errorDto = new ErrorDto(LocalDateTime.now(), ex.getMessage(), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDto, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, String>> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    protected ResponseEntity<ErrorDto> handleBadRequest(
            RuntimeException ex) {
        ErrorDto errorDto = new ErrorDto(LocalDateTime.now(), ex.getMessage(), ex.getClass().getSimpleName());
        return new ResponseEntity<>(errorDto, HttpStatus.BAD_REQUEST);
    }

}
