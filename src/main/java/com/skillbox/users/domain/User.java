package com.skillbox.users.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
@SQLDelete(sql = "UPDATE users SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "id", length = 36)
    private UUID id;

    @Column(name = "first_name", length = 100, nullable = false)
    private String firstName;

    @Column(name = "middle_name", length = 100)
    private String middleName;

    @Column(name = "last_name", length = 100, nullable = false)
    private String lastName;

    @Column(name = "gender", length = 6, nullable = false)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "birthday", nullable = false)
    private LocalDate birthday;

    @ManyToOne
    @JoinColumn(name = "city_id", nullable = false)
    private City city;

    @Column(name = "avatar_url", length = 250)
    private String avatarUrl;

    @Column(name = "additional_info", length = 500)
    private String additionalInfo;

    @Column(name = "nickname", length = 100, nullable = false)
    private String nickname;

    @Column(name = "email", length = 100, nullable = false)
    private String email;

    @Column(name = "phone", length = 13, nullable = false)
    private String phone;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_skills",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id"))
    private List<Skill> skills = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "subscribers",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "subscribed_to_user_id"))
    private List<User> subscribedTo = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "subscribers",
            joinColumns = @JoinColumn(name = "subscribed_to_user_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private List<User> subscribers = new ArrayList<>();

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;

    public User(String firstName, String middleName, String lastName,
                Gender gender, LocalDate birthday, City city,
                String additionalInfo, String nickname, String email,
                String phone) {
        this(firstName, lastName, gender, birthday, city, additionalInfo, nickname, email, phone);
        this.middleName = middleName;
    }

    public User(String firstName, String middleName, String lastName,
                Gender gender, LocalDate birthday, City city,
                String nickname, String email,
                String phone) {
        this(firstName, lastName, gender, birthday, city, "", nickname, email, phone);
        this.middleName = middleName;
    }

    public User(String firstName, String lastName,
                Gender gender, LocalDate birthday, City city,
                String nickname, String email,
                String phone) {
        this(firstName, lastName, gender, birthday, city, "", nickname, email, phone);
    }

    public User(String firstName, String lastName,
                Gender gender, LocalDate birthday, City city,
                String additionalInfo, String nickname, String email,
                String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;
        this.city = city;
        this.additionalInfo = additionalInfo;
        this.nickname = nickname;
        this.email = email;
        this.phone = phone;
    }

    @Override
    public String toString() {
        Set<UUID> subscribedToNames = subscribedTo.stream().map(user -> user.getId()).collect(Collectors.toSet());
        Set<UUID> subscribersNames = subscribers.stream().map(user -> user.getId()).collect(Collectors.toSet());

        return "User{"
                + "id=" + id
                + ", firstName='" + firstName + '\''
                + ", middleName='" + (middleName == null ? "" : middleName) + '\''
                + ", lastName='" + lastName + '\''
                + ", gender=" + gender
                + ", birthDay=" + birthday
                + ", city=" + city
                + ", nickName='" + nickname + '\''
                + ", email='" + email + '\''
                + ", phone='" + phone + '\''
                + ", skills=" + skills
                + ", subscribedTo=" + subscribedToNames
                + ", subscribers=" + subscribersNames
                + '}';
    }
}
