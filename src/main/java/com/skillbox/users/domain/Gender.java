package com.skillbox.users.domain;

public enum Gender {
    MALE,
    FEMALE
}
