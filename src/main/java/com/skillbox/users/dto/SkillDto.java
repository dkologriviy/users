package com.skillbox.users.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class SkillDto {

    @NotNull(message = "Поле нужно заполнить!")
    private final UUID id;

    @NotBlank(message = "Поле нужно заполнить!")
    private final String name;

    public SkillDto(@JsonProperty("id") UUID id, @JsonProperty("name") String name) {
        this.id = id;
        this.name = name;
    }
}
