package com.skillbox.users.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class UnsubscribeDto {

    @NotNull(message = "Поле нужно заполнить!")
    private UUID userId;

    @NotNull(message = "Поле нужно заполнить!")
    private UUID unsubscribeFromUserId;
}
