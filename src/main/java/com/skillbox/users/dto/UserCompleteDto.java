package com.skillbox.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.skillbox.users.domain.Gender;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class UserCompleteDto {

    private UUID id;

    private String firstName;

    private String middleName;

    private String lastName;

    private Gender gender;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    private CityDto city;

    private String avatarUrl;

    private String additionalInfo;

    private String nickname;

    private String email;

    private String phone;

    private List<SkillDto> skills = new ArrayList<>();

    private List<UserDto> subscribedTo = new ArrayList<>();

    private List<UserDto> subscribers = new ArrayList<>();

}
