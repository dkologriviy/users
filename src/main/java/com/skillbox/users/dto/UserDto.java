package com.skillbox.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.skillbox.users.domain.Gender;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class UserDto {

    @NotNull(message = "Поле нужно заполнить!")
    private UUID id;

    @NotBlank(message = "Поле нужно заполнить!")
    private String firstName;

    @NotBlank(message = "Поле нужно заполнить!")
    private String middleName;

    @NotBlank(message = "Поле нужно заполнить!")
    private String lastName;

    @NotNull(message = "Поле нужно заполнить!")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Past(message = "Поле нужно заполнить!")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @NotNull(message = "Поле нужно заполнить!")
    private CityDto city;

    private String avatarUrl;

    private String additionalInfo;

    @NotBlank(message = "Поле нужно заполнить!")
    private String nickname;

    @NotBlank(message = "Поле нужно заполнить!")
    private String email;

    @NotBlank(message = "Поле нужно заполнить!")
    private String phone;

}
