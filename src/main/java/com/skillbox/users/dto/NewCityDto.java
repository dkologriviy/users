package com.skillbox.users.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class NewCityDto {

    @NotBlank(message = "Поле нужно заполнить!")
    private String name;
}
