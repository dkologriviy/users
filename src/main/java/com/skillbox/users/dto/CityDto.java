package com.skillbox.users.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CityDto {
    @NotNull(message = "Поле нужно заполнить!")
    private UUID id;

    @NotBlank(message = "Поле нужно заполнить!")
    private String name;
}
