package com.skillbox.users.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class NewSkillDto {

    @NotBlank(message = "Поле нужно заполнить!")
    private String name;
}
