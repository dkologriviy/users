package com.skillbox.users.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.skillbox.users.domain.Gender;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class NewUserDto {

    @NotBlank(message = "Поле нужно заполнить!")
    private String firstName;

    @NotBlank(message = "Поле нужно заполнить!")
    private String middleName;

    @NotBlank(message = "Поле нужно заполнить!")
    private String lastName;

    @NotNull(message = "Поле нужно заполнить!")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull(message = "Поле нужно заполнить!")
    @Past(message = "Дата не может быть больше текущей")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;

    @NotNull(message = "Поле нужно заполнить!")
    private UUID cityId;

    private String avatarUrl;

    private String additionalInfo;

    @NotBlank(message = "Поле нужно заполнить!")
    private String nickname;

    @NotBlank(message = "Поле нужно заполнить!")
    private String email;

    @NotBlank(message = "Поле нужно заполнить!")
    private String phone;
}
