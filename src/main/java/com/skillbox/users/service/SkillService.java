package com.skillbox.users.service;

import com.skillbox.users.domain.Skill;
import com.skillbox.users.domain.User;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.SkillRepository;
import com.skillbox.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class SkillService {

    private final SkillRepository skillRepository;
    private final UserRepository userRepository;

    public List<Skill> getAll() {
        log.debug("getAll() - start");
        List<Skill> skills = skillRepository.findAll();
        log.debug("getAll() - end");
        return skills;
    }

    public Skill getById(UUID skillId) {
        log.debug("getById() - start: skillId={}", skillId);
        Skill skill = skillRepository.findById(skillId).orElseThrow(
                () -> {
                    log.debug("getById() - end: skill with id={} does not exist", skillId);
                    return new ObjectNotFoundException("Навык с указанным id не существует");
                }
        );
        log.debug("getById() - end");
        return skill;
    }

    public void delete(UUID skillId) {
        log.debug("delete() - start: skillId={}", skillId);
        Skill skillToRemove = getById(skillId);

        List<User> users = userRepository.getUsersBySkillId(skillId);
        users.forEach(user -> user.getSkills().remove(skillToRemove));
        userRepository.saveAll(users);
        skillRepository.delete(skillToRemove);
        log.debug("delete() - end");
    }

    public Skill add(String skillName) {
        log.debug("add() - start: skillName={}", skillName);
        String preparedSkillName = skillName.toLowerCase();
        skillRepository.findSkillByName(preparedSkillName).ifPresent(skill -> {
            log.debug("add() - end: skill with name={} already exists", preparedSkillName);
            throw new ObjectAlreadyExistException("Такой навык уже есть в справочнике!");
        });
        Skill skill = new Skill(preparedSkillName);
        log.debug("add() - end");
        return skillRepository.save(skill);
    }
}
