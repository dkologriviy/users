package com.skillbox.users.service;

import com.skillbox.users.domain.City;
import com.skillbox.users.domain.User;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.CityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class CityService {
    private final CityRepository cityRepository;
    private final UserService userService;

    private static final String ID_ALREADY_EXIST_ERROR_MESSAGE = "Город с указанным id не существует";

    public List<City> getAll() {
        log.debug("getAll() - start");
        List<City> cities = cityRepository.findAll();
        log.debug("getAll() - end");
        return cities;
    }

    public City add(String name) {
        log.debug("add() - start: name={}", name);
        cityRepository.findCityByName(name).ifPresent((city -> {
            log.debug("add() - end: city already exist");
            throw new ObjectAlreadyExistException("Такой город уже существует!");
        }));
        City newCity = new City(name);
        log.debug("add() - end");
        return cityRepository.save(newCity);
    }

    public void delete(UUID cityId) {
        log.debug("delete() - start: cityId={}", cityId);
        City city = cityRepository.findById(cityId).orElseThrow(
                () -> {
                    log.debug("delete() - end: city with id={} cannot be found", cityId);
                    return new ObjectNotFoundException(ID_ALREADY_EXIST_ERROR_MESSAGE);
                }
        );
        List<User> users = userService.getUsersByCityId(cityId);
        if (users.isEmpty()) {
            cityRepository.delete(city);
            log.debug("delete() - end");
        }
    }

    public String getNameById(UUID cityId) {
        log.debug("getNameById() - start: cityId={}", cityId);
        City city = cityRepository.findById(cityId).orElseThrow(
                () -> {
                    log.debug("getNameById() - end: city with id={} cannot be found", cityId);
                    return new ObjectNotFoundException(ID_ALREADY_EXIST_ERROR_MESSAGE);
                }
        );
        log.debug("getNameById() - end");
        return city.getName();
    }

    public City getById(UUID cityId) {
        log.debug("getById() - start: cityId={}", cityId);
        City city = cityRepository.findById(cityId).orElseThrow(
                () -> {
                    log.debug("getById() - end: city with id={} cannot be found", cityId);
                    return new ObjectNotFoundException(ID_ALREADY_EXIST_ERROR_MESSAGE);
                }
        );
        log.debug("getById() - end");
        return city;
    }
}
