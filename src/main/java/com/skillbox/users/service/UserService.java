package com.skillbox.users.service;

import com.skillbox.users.domain.Skill;
import com.skillbox.users.domain.User;
import com.skillbox.users.exception.ObjectAlreadyExistException;
import com.skillbox.users.exception.ObjectNotFoundException;
import com.skillbox.users.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;

    private final SkillService skillService;

    public List<User> getUsersByCityId(UUID cityId) {
        log.debug("getUsersByCityId() - start: cityId={}", cityId);
        List<User> users = userRepository.getUsersByCityId(cityId);
        log.debug("getUsersByCityId() - end");
        return users;
    }

    public User getById(UUID userId) {
        log.debug("getById() - start: userId={}", userId);
        User user = userRepository.findById(userId).orElseThrow(
                () -> {
                    log.debug("getById() - end: user with id = {} does not exist", userId);
                    return new ObjectNotFoundException("Пользователя с указанным id не существует:" + userId);
                }
        );
        log.debug("getById() - end");
        return user;
    }

    public List<User> getAll() {
        log.debug("getAll() - start");
        List<User> users = userRepository.findAll();
        log.debug("getAll() - end");
        return users;
    }

    public User add(User user) {
        log.debug("add() - start: user={}", user);
        userRepository.findUserByEmail(user.getEmail()).ifPresent(existUser -> {
            log.debug("add() - end: user with email={} is already registered", user.getEmail());
            throw new ObjectAlreadyExistException("Пользователь с таким email уже зарегистрирован!");
        });
        userRepository.save(user);
        log.debug("add() - end");
        return user;
    }

    public User update(User user) {
        log.debug("update() - start: user={}", user);
        userRepository.findUserByEmail(user.getEmail()).ifPresent(existUser -> {
            if (existUser.getId() != user.getId()) {
                log.debug("update() - end: email {} is already used by another user", user.getEmail());
                throw new ObjectAlreadyExistException("Указанный email уже используется другим пользователем!");
            }
        });

        userRepository.save(user);
        log.debug("update - end");
        return user;
    }

    public User subscribeTo(UUID userId, UUID subscribeToUserId) {
        log.debug("subscribeTo() - start: userId={}, subscribeToUserId={}", userId, subscribeToUserId);
        User user = getById(userId);
        User subscribeToUser = getById(subscribeToUserId);

        if (user.getSubscribedTo().contains(subscribeToUser)) {
            log.debug("subscribeTo() - end: user userId={} is already subscribed to user subscribeToUserId={}", userId, subscribeToUserId);
            throw new ObjectAlreadyExistException(
                    String.format("Пользователь %s уже подписан на пользователя %s", userId, subscribeToUserId));
        }

        user.getSubscribedTo().add(subscribeToUser);
        userRepository.save(user);
        log.debug("subscribeTo() - end");
        return user;
    }

    public User unsubscribe(UUID userId, UUID unsubscribeFromUserId) {
        log.debug("unsubscribe() - start: userId={}, unsubscribeFromUserId={}", userId, unsubscribeFromUserId);
        User user = getById(userId);
        User unsubscribeFromUser = getById(unsubscribeFromUserId);
        if (!user.getSubscribedTo().contains(unsubscribeFromUser)) {
            log.debug("unsubscribe() - end: user userId={} is not subscribed to user unsubscribeFromUserId={}",
                    userId, unsubscribeFromUserId);
            throw new ObjectNotFoundException(String.format("Пользователь %s не подписан на пользователя %s",
                    userId, unsubscribeFromUserId));
        }
        user.getSubscribedTo().remove(unsubscribeFromUser);
        userRepository.save(user);
        log.debug("unsubscribe() - end");
        return user;
    }

    public User addSkill(UUID userId, UUID skillId) {
        log.debug("addSkill() - start: userId={}, skillId={}", userId, skillId);
        User user = getById(userId);
        Skill skill = skillService.getById(skillId);
        if (user.getSkills().contains(skill)) {
            log.debug("addSkill() - end: user userId={} is already have skill skillId={}", userId, skillId);
            throw new ObjectAlreadyExistException(String.format("У пользователя %s уже добавлен навык %s", userId, skillId));
        }
        user.getSkills().add(skill);
        userRepository.save(user);
        log.debug("addSkill() - end");
        return user;
    }

    public User deleteSkill(UUID userId, UUID skillId) {
        log.debug("deleteSkill() - start: userId={}, skillId={}", userId, skillId);
        User user = getById(userId);
        Skill skill = skillService.getById(skillId);
        if (!user.getSkills().contains(skill)) {
            log.debug("deleteSkill() - end: user userId={} does not have skill skillId={}", userId, skillId);
            throw new ObjectNotFoundException(String.format("У пользователя %s нет навыка %s", userId, skillId));
        }
        user.getSkills().remove(skill);
        userRepository.save(user);
        log.debug("deleteSkill() - end");
        return user;
    }
}
