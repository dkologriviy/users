docker build -t users:1.0.0 .
kubectl create namespace kologriviy-dev
kubectl create namespace kologriviy-feature
kubectl create namespace kologriviy-preprod
helm install users-dev . -f values-dev.yaml
helm install users-preprod . -f values-preprod.yaml
kubectl port-forward service/users-backend-service 8080:8080 --namespace=kologriviy-dev
kubectl port-forward service/users-backend-service 8080:8080 --namespace=kologriviy-preprod