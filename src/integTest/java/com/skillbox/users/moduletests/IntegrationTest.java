package com.skillbox.users.moduletests;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ActiveProfiles("integrationTest")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ContextConfiguration(initializers = IntegrationTest.Initializer.class)
public @interface IntegrationTest {
    class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        private static final PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres:13.3");

        static {
            postgreSQLContainer.withDatabaseName("users")
                    .withUsername("db_admin")
                    .withPassword("db_password")
                    .withInitScript("databaseInitScripts/schemaCreate.sql")
                    .withUrlParam("currentSchema", "users_schema");
            postgreSQLContainer.withExposedPorts(5432);
            postgreSQLContainer.start();
        }

        private static String[] springApplicationProperties() {
            return new String[]{
                "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                "spring.datasource.password=" + postgreSQLContainer.getPassword(),
                "spring.liquibase.url=" + postgreSQLContainer.getJdbcUrl(),
                "spring.liquibase.user=" + postgreSQLContainer.getUsername(),
                "spring.liquibase.password=" + postgreSQLContainer.getPassword()
            };
        }

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertyValues
                    .of(springApplicationProperties())
                    .applyTo(applicationContext.getEnvironment());
        }
    }
}
