package com.skillbox.users.moduletests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.skillbox.users.domain.City;
import com.skillbox.users.domain.Gender;
import com.skillbox.users.domain.Skill;
import com.skillbox.users.dto.NewUserDto;
import com.skillbox.users.dto.SubscribeToDto;
import com.skillbox.users.dto.UnsubscribeDto;
import com.skillbox.users.dto.UserCompleteDto;
import com.skillbox.users.dto.UserDto;
import com.skillbox.users.dto.UserSkillDto;
import com.skillbox.users.moduletests.utils.TestUtils;
import com.skillbox.users.service.CityService;
import com.skillbox.users.service.SkillService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserResourceModuleTest {

    @Autowired
    CityService cityService;

    @Autowired
    SkillService skillService;

    @Autowired
    protected MockMvc mockMvc;

    static NewUserDto newUserDto1;
    static NewUserDto newUserDto2;
    static UserCompleteDto userCompleteDto1;
    static UserCompleteDto userCompleteDto2;


    static Skill skill1;
    static Skill skill2;
    static City city;
    static UserSkillDto userSkillDto;
    static SubscribeToDto subscribeToDto;
    static UnsubscribeDto unsubscribeDto;

    ObjectMapper objectMapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();


    @BeforeAll
    void initDbData() {
        city = cityService.add("Moscow");
        skill1 = skillService.add("Java");
        skill2 = skillService.add("DevOps");

        newUserDto1 = new NewUserDto("Ivan", "Ivanovich", "Ivanov", Gender.FEMALE, LocalDate.of(1988, 1, 1),
                city.getId(), "http://vk.com/user123/avatar.jpg", "I'm user one", "Vano", "iivanov@gmail.com", "+79166543210");

        newUserDto2 = new NewUserDto("Petr", "Petrovich", "Petrov", Gender.FEMALE, LocalDate.of(1990, 1, 1),
                city.getId(), "http://vk.com/user777/avatar.jpg", "I'm user two", "Petya", "ppetrov@gmail.com", "+79161234567");

    }

    @Test
    @Order(1)
    void addSuccessfullyTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/user/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(newUserDto1)))
                .andExpect(status().isOk());
        userCompleteDto1 = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertEquals(newUserDto1.getAvatarUrl(), userCompleteDto1.getAvatarUrl());
        assertEquals(newUserDto1.getAdditionalInfo(), userCompleteDto1.getAdditionalInfo());
        assertEquals(newUserDto1.getPhone(), userCompleteDto1.getPhone());
        assertEquals(newUserDto1.getEmail(), userCompleteDto1.getEmail());
        assertEquals(newUserDto1.getFirstName(), userCompleteDto1.getFirstName());
        assertEquals(newUserDto1.getMiddleName(), userCompleteDto1.getMiddleName());
        assertEquals(newUserDto1.getLastName(), userCompleteDto1.getLastName());
        assertEquals(newUserDto1.getBirthday(), userCompleteDto1.getBirthday());
        assertEquals(newUserDto1.getCityId(), userCompleteDto1.getCity().getId());

        ResultActions resultActions2 = mockMvc.perform(MockMvcRequestBuilders.post("/user/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(newUserDto2)))
                .andExpect(status().isOk());

        userCompleteDto2 = TestUtils.resultActionsToObject(resultActions2, UserCompleteDto.class);
    }

    @Test
    @Order(2)
    void addForSecondTimeErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/user/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(newUserDto1)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectAlreadyExistException")));
    }

    @Test
    @Order(3)
    void getUserByIdSuccessfullyTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/{userId}", userCompleteDto1.getId())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.equalTo(userCompleteDto1.getId().toString())));
    }

    @Test
    @Order(4)
    void getUserByIdErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/{userId}", UUID.randomUUID())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Order(5)
    void getUserGetAllSuccessfullyTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get("/user/getAll")
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());

        List<UserCompleteDto> userCompleteDtoList = TestUtils.resultActionsToObjectList(resultActions, UserCompleteDto.class);

        assertTrue(userCompleteDtoList.contains(userCompleteDto1));
        assertTrue(userCompleteDtoList.contains(userCompleteDto2));
    }

    @Test
    @Order(6)
    void addSkillSuccessfullyTest() throws Exception {
        userSkillDto = new UserSkillDto();
        userSkillDto.setSkillId(skill1.getId());
        userSkillDto.setUserId(userCompleteDto1.getId());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/user/skill")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userSkillDto)))
                .andExpect(status().isOk());

        UserCompleteDto userResult = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertTrue(userResult.getSkills().stream().anyMatch(skill -> skill.getId().equals(skill1.getId())));
    }

    @Test
    @Order(7)
    void addSkillErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/user/skill")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userSkillDto)))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    @Order(8)
    void deleteSkillSuccessfullyTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/user/skill")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userSkillDto)))
                .andExpect(status().isOk());

        UserCompleteDto userResult = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertFalse(userResult.getSkills().stream().anyMatch(skill -> skill.getId().equals(skill1.getId())));
    }

    @Test
    @Order(9)
    void deleteSkillErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/skill")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userSkillDto)))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectNotFoundException")));
    }

    @Test
    @Order(10)
    void subscribeToSuccessfullyTest() throws Exception {
        subscribeToDto = new SubscribeToDto();
        subscribeToDto.setUserId(userCompleteDto1.getId());
        subscribeToDto.setSubscribeToUserId(userCompleteDto2.getId());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/user/subscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(subscribeToDto)))
                .andExpect(status().isOk());

        UserCompleteDto userResult = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertTrue(userResult.getSubscribedTo().stream().anyMatch(subscribedTo -> subscribedTo.getId().equals(userCompleteDto2.getId())));
    }

    @Test
    @Order(11)
    void subscribeToErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/user/subscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(subscribeToDto)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectAlreadyExistException")));

        SubscribeToDto subscribeToNotExistUser = new SubscribeToDto();
        subscribeToNotExistUser.setUserId(userCompleteDto1.getId());
        subscribeToNotExistUser.setSubscribeToUserId(UUID.randomUUID());
        mockMvc.perform(MockMvcRequestBuilders.post("/user/subscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(subscribeToNotExistUser)))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectNotFoundException")));

        SubscribeToDto subscribeByNotExistUser = new SubscribeToDto();
        subscribeByNotExistUser.setUserId(UUID.randomUUID());
        subscribeByNotExistUser.setSubscribeToUserId(userCompleteDto2.getId());
        mockMvc.perform(MockMvcRequestBuilders.post("/user/subscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(subscribeByNotExistUser)))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectNotFoundException")));
    }

    @Test
    @Order(12)
    void unsubscribeSuccessfullyTest() throws Exception {
        unsubscribeDto = new UnsubscribeDto();
        unsubscribeDto.setUserId(userCompleteDto1.getId());
        unsubscribeDto.setUnsubscribeFromUserId(userCompleteDto2.getId());

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/user/unsubscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(unsubscribeDto)))
                .andExpect(status().isOk());

        UserCompleteDto userResult = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertFalse(userResult.getSubscribedTo().stream().anyMatch(subscribedTo -> subscribedTo.getId().equals(userCompleteDto2.getId())));
    }

    @Test
    @Order(13)
    void unsubscribeErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/user/unsubscribe")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(unsubscribeDto)))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectNotFoundException")));
    }

    @Test
    @Order(14)
    void updateSuccessfullyTest() throws Exception {
        UserDto userForUpdate = new UserDto();
        userForUpdate.setId(userCompleteDto1.getId());
        userForUpdate.setAdditionalInfo("I'm updated user one");
        userForUpdate.setAvatarUrl("http://vk.com/updated_avatar.jpg");
        userForUpdate.setBirthday(LocalDate.of(1990, 10, 10));
        userForUpdate.setCity(userCompleteDto1.getCity());
        userForUpdate.setEmail("new-email@ya.ru");
        userForUpdate.setFirstName("Sidr");
        userForUpdate.setMiddleName("Sidorovich");
        userForUpdate.setLastName("Sidorov");
        userForUpdate.setNickname("new-nickname");
        userForUpdate.setPhone("+7986543210");
        userForUpdate.setGender(Gender.MALE);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.put("/user/update")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userForUpdate)))
                .andExpect(status().isOk());

        UserCompleteDto updatedUserCompleteDto = TestUtils.resultActionsToObject(resultActions, UserCompleteDto.class);
        assertEquals(userForUpdate.getAvatarUrl(), updatedUserCompleteDto.getAvatarUrl());
        assertEquals(userForUpdate.getAdditionalInfo(), updatedUserCompleteDto.getAdditionalInfo());
        assertEquals(userForUpdate.getPhone(), updatedUserCompleteDto.getPhone());
        assertEquals(userForUpdate.getEmail(), updatedUserCompleteDto.getEmail());
        assertEquals(userForUpdate.getFirstName(), updatedUserCompleteDto.getFirstName());
        assertEquals(userForUpdate.getMiddleName(), updatedUserCompleteDto.getMiddleName());
        assertEquals(userForUpdate.getLastName(), updatedUserCompleteDto.getLastName());
        assertEquals(userForUpdate.getBirthday(), updatedUserCompleteDto.getBirthday());
        assertEquals(userForUpdate.getCity().getId(), updatedUserCompleteDto.getCity().getId());
    }

    @Test
    @Order(15)
    void updateErrorTest() throws Exception {
        UserDto userForUpdate = new UserDto();
        userForUpdate.setId(userCompleteDto1.getId());
        userForUpdate.setAdditionalInfo("I'm updated user one");
        userForUpdate.setAvatarUrl("http://vk.com/updated_avatar.jpg");
        userForUpdate.setBirthday(LocalDate.of(1990, 10, 10));
        userForUpdate.setCity(userCompleteDto1.getCity());
        //email is not set! userForUpdate.setEmail("new-email@ya.ru");
        userForUpdate.setFirstName("Sidr");
        userForUpdate.setMiddleName("Sidorovich");
        userForUpdate.setLastName("Sidorov");
        userForUpdate.setNickname("new-nickname");
        userForUpdate.setPhone("+7986543210");
        userForUpdate.setGender(Gender.MALE);

        mockMvc.perform(MockMvcRequestBuilders.put("/user/update")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(userForUpdate)))
                .andExpect(status().isBadRequest());
    }
}
