package com.skillbox.users.moduletests;

import com.skillbox.users.dto.SkillDto;
import com.skillbox.users.moduletests.utils.TestUtils;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SkillResourceModuleTest {

    @Autowired
    protected MockMvc mockMvc;

    static SkillDto existSkill;

    @Test
    @Order(1)
    void addSuccessfullyTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/skill/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/mvc-requests/skills/newSkill1.json"),
                                StandardCharsets.UTF_8)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.equalTo("java developer")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()));

        existSkill = TestUtils.resultActionsToObject(resultActions, SkillDto.class);
    }

    @Test
    @Order(2)
    void addForSecondTimeErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/skill/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/mvc-requests/skills/newSkill1.json"),
                                StandardCharsets.UTF_8)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectAlreadyExistException")))
        ;
    }

    @Test
    @Order(3)
    void getAllSuccessfullyTest() throws Exception {
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/skill/getAll"))
                .andExpect(status().isOk());

        List<SkillDto> skillDtoList = TestUtils.resultActionsToObjectList(result, SkillDto.class);

        assertEquals(1, skillDtoList.size());
        assertTrue(skillDtoList.contains(existSkill));
    }

    @Test
    @Order(4)
    void deleteSkillByIdSuccessfullyTest() throws Exception {
        //delete for the first time
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/skill/{skillIdToDelete}", existSkill.getId())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        UUID deletedCityUuid = TestUtils.resultActionsToObject(resultActions, UUID.class);
        assertEquals(existSkill.getId(), deletedCityUuid);
    }

    @Test
    @Order(5)
    void deleteSkillForSecondTimeErrorTest() throws Exception {
        //delete for the second time
        mockMvc.perform(MockMvcRequestBuilders.delete("/skill/{skillIdToDelete}", existSkill.getId())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }
}
