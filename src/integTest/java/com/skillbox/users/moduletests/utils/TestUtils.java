package com.skillbox.users.moduletests.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.io.UnsupportedEncodingException;
import java.util.List;

public abstract class TestUtils {

    private static final ObjectMapper objectMapper = JsonMapper.builder()
            .addModule(new JavaTimeModule())
            .build();

    public static <T> T resultActionsToObject(ResultActions resultActions, Class<T> t)
            throws UnsupportedEncodingException, JsonProcessingException {
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        return objectMapper.readValue(contentAsString, t);
    }

    public static <T> List<T> resultActionsToObjectList(ResultActions resultActions, Class<T> t)
            throws UnsupportedEncodingException, JsonProcessingException {

        JavaType type = objectMapper.getTypeFactory().constructParametricType(List.class, t);
        return objectMapper.readValue(resultActions.andReturn()
                .getResponse().getContentAsString(), type);
    }
}
