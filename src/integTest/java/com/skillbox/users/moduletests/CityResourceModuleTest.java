package com.skillbox.users.moduletests;

import com.skillbox.users.dto.CityDto;
import com.skillbox.users.moduletests.utils.TestUtils;
import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CityResourceModuleTest {

    static CityDto existCity;

    @Autowired
    protected MockMvc mockMvc;

    @Test
    @Order(1)
    void addSuccessfullyTest() throws Exception {
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.post("/city/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/mvc-requests/cities/newCity1.json"),
                                StandardCharsets.UTF_8)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.equalTo("Samara")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()));

        existCity = TestUtils.resultActionsToObject(resultActions, CityDto.class);
    }

    @Test
    @Order(2)
    void addForSecondTimeErrorTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/city/add")
                        .contentType(APPLICATION_JSON_VALUE)
                        .content(IOUtils.toString(getClass().getResourceAsStream("/mvc-requests/cities/newCity1.json"),
                                StandardCharsets.UTF_8)))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorCode", Matchers.equalTo("ObjectAlreadyExistException")))
        ;
    }

    @Test
    @Order(3)
    void getAllSuccessfullyTest() throws Exception {
        var result = mockMvc.perform(MockMvcRequestBuilders.get("/city/getAll"))
                .andExpect(status().isOk());

        List<CityDto> cityDtoList = TestUtils.resultActionsToObjectList(result, CityDto.class);

        assertEquals(1, cityDtoList.size());
        assertTrue(cityDtoList.contains(existCity));
    }

    @Test
    @Order(4)
    void deleteCityByIdSuccessfullyTest() throws Exception {
        //delete for the first time
        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.delete("/city/{cityIdToDelete}", existCity.getId())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        UUID deletedCityUuid = TestUtils.resultActionsToObject(resultActions, UUID.class);
        assertEquals(existCity.getId(), deletedCityUuid);
    }

    @Test
    @Order(5)
    void deleteCityForSecondTimeErrorTest() throws Exception {
        //delete for the second time
        mockMvc.perform(MockMvcRequestBuilders.delete("/city/{cityIdToDelete}", existCity.getId())
                        .contentType(APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }
}
